class Header extends HTMLElement {
  constructor() {
    super();
  }

  connectedCallback() {
    this.innerHTML =
      '<h1><a href="/breadblog/">Bread Blog</a></h1> <div> <a href="/breadblog/pro/pro.html">Posts</a> </div>' +
      '<div> <a href="/breadblog/gamers/gamers.html">Games</a> </div> <div> <a href="/breadblog/about.html">About</a>';
  }
}

customElements.define("header-comp", Header);
